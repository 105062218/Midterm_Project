function init() {
    var user = firebase.database().currentUser;
    var amount = 1;
    var additem1 = document.getElementById('item1');

    document.getElementById('amount1').addEventListener('change', function(){
        amount = document.getElementById('amount1').value;
    });

    var menu = document.getElementById('dynamic-menu');

    firebase.auth().onAuthStateChanged(function (user) {
        
        if (user) {
            menu.innerHTML = "<span class='dropdown-item' id='profilePage'>" + user.email+"</span><span class='dropdown-item' id='shopcart'>Shopping Cart</span>" + "<span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            document.getElementById('shopcart').addEventListener('click', function(){window.location = 'cart.html'});
            document.getElementById('profilePage').addEventListener('click', function(){window.location = 'profile.html'});
            additem1.addEventListener('click', function(){
                var cartRef = firebase.database().ref('shoppingCart/'+user.uid+'/item2');
                cartRef.set({name: 'item2', amount: eval(amount), price: 139}).then(function(){alert('item2 has added to the cart')});
                /*var userRef = firebase.database().ref('com_list/'+user.uid);
                var s;
                if(userRef.sum==undefined||userRef.sum==null) s=0;
                else s=userRef.sum;
                s += eval(amount)*120;
                userRef.update({sum: s});*/
                
            });

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            additem1.classList.add('disabled');
            additem1.innerHTML = 'Login to Buy';
        }
    });

    if(user){
        menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
        var logout_button = document.getElementById('logout-btn');
        logout_button.addEventListener('click', function () {
            firebase.auth().signOut()
                .then(function () {
                    alert('Sign Out!')
                })
                .catch(function (error) {
                    alert('Sign Out Error!')
                });
        });

        additem1.addEventListener('click', function(){
            var cartRef = firebase.database().ref('shoppingCart/'+user.uid+'/item2');
            cartRef.set({name: 'item2', amount: eval(amount), price: 139}).then(function(){alert('item2 has added to the cart')});
        });
    }else{
        menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
    }


}

window.onload = function () {
    init();
}