function init() {
    var ID;
    var u = firebase.auth().currentUser;
    var menu = document.getElementById('dynamic-menu');
    var checkout_btn = document.getElementById('checkout_btn');
    var total = 0;

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            ID = user.uid;
            u = user;
            menu.innerHTML = "<span class='dropdown-item'  id='profilePage'>" + user.email+"</span><span class='dropdown-item' id='shopcart'>Shopping Cart</span>" + "<span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            document.getElementById('shopcart').addEventListener('click', function(){window.location = 'cart.html'});
            document.getElementById('profilePage').addEventListener('click', function(){window.location = 'profile.html'});
            var postsRef = firebase.database().ref('shoppingCart/'+u.uid);
            var total_post = [];
            var first_count = 0;
            var second_count = 0;
            postsRef.once('value')
                .then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        console.log(childData);
                        if(childData.amount!=0)total_post[total_post.length] = "<a href='"+childData.name+".html' class='list-group-item d-flex justify-content-between align-items-center'>"+childData.name+"<span class='badge badge-primary badge-pill'>"+childData.amount+"</span><span class='badge cyan'>$"+childData.price+"</span></a>";
                        //if(childData.name==undefined) total_post[total_post.length-1] = childData;
                        total += eval(childData.amount+'*'+childData.price);
                        first_count += 1;
                    });
                    document.getElementById('post_list').innerHTML = total_post.join('');
                    if(total!=0) document.getElementById('post_list').innerHTML += "<a class='list-group-item d-flex justify-content-between align-items-center'>Total : $"+total+"</a>";
                    else{
                        document.getElementById('post_list').innerHTML += "<a class='list-group-item d-flex justify-content-between align-items-center'>No items in the cart</a>";
                        checkout_btn.classList.add('disabled');
                    }
                    //add listener
                    postsRef.on('child_added', function (data) {
                        second_count += 1;
                        if (second_count > first_count) {
                            var childData = data.val();
                            console.log(childData);
                            total_post[total_post.length] = childData.name+' '+childData.amount+' '+childData.price;
                            document.getElementById('post_list').innerHTML = total_post.join('');
                        }
                    });
                })
                .catch(e => console.log(e.message));

                checkout_btn.addEventListener('click', function(){window.location = 'checkout.html';});

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = '';
            window.location = 'index.html';
        }

    });

    /*if (u) {
        ID = u.uid;
        menu.innerHTML = "<span class='dropdown-item'>" + u.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
        var logout_button = document.getElementById('logout-btn');
        logout_button.addEventListener('click', function () {
            firebase.auth().signOut()
                .then(function () {
                    alert('Sign Out!')
                })
                .catch(function (error) {
                    alert('Sign Out Error!')
                });
        });
    } else {
        menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
    }*/


    /*console.log(u);
    var postsRef = firebase.database().ref('shoppingCart/'+ID);
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = childData.name + "</strong>" + childData.price + childData.amount;
                first_count += 1;
            });
            document.getElementById('post_list').innerHTML = total_post.join('');
            console.log(total_post);

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = childData.name + "</strong>" + childData.price + childData.amount;
                    document.getElementById('post_list').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));*/
}

window.onload = function () {
    init();
}