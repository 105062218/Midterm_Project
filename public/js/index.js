function init() {
    var user_email = '';
    var ID = '';
    var cu;
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        if (user) {
            user_email = user.email;
            ID = user.uid;
            cu = user;
            menu.innerHTML = "<span class='dropdown-item' id='profilePage'>" + user_email+"</span><span class='dropdown-item' id='shopcart'>Shopping Cart</span>" + "<span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            document.getElementById('shopcart').addEventListener('click', function(){window.location = 'cart.html'});
            document.getElementById('profilePage').addEventListener('click', function(){window.location = 'profile.html'});

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            
        }

            
    });
    //var cu = firebase.auth().currentUser;
    /*post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');
    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value
            });
            post_txt.value = "";
        }
    });*/

}

window.onload = function () {
    init();
}