function init() {
    var ID;
    var menu = document.getElementById('dynamic-menu');
    var back_btn = document.getElementById('back_btn');
    var send_btn = document.getElementById('send_btn');
    var total = 0;
    var name = document.getElementById('name');
    var addr = document.getElementById('addr');
    var email = document.getElementById('email');
    var phone = document.getElementById('phone');
    var inputName = name.value;
    var inputEmail = email.value;
    var inputAddr = addr.value;
    var inputPhone = phone.value;
    var amount1 = 0;
    var amount2 = 0;
    var amount3 = 0;

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            ID = user.uid;
            email.value = user.email;
            menu.innerHTML = "<span class='dropdown-item'  id='profilePage'>" + user.email+"</span><span class='dropdown-item' id='shopcart'>Shopping Cart</span>" + "<span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            document.getElementById('shopcart').addEventListener('click', function(){window.location = 'cart.html'});
            back_btn.addEventListener('click', function(){window.location = 'cart.html'});
            document.getElementById('profilePage').addEventListener('click', function(){window.location = 'profile.html'});

            var userRef = firebase.database().ref('UserName/'+ID);
            userRef.once('value').then(function(snapshot){
                name.value = snapshot.val().name;
                inputName = snapshot.val().name;
                addr.value = snapshot.val().address;
                inputAddr = snapshot.val().address;
                phone.value = snapshot.val().phone;
                inputPhone = snapshot.val().phone;
            });

            inputName = name.value;
            inputEmail = email.value;
            inputAddr = addr.value;
            inputPhone = phone.value;

            name.addEventListener('change', function(){inputName = name.value});
            email.addEventListener('change', function(){inputEmail = email.value});
            addr.addEventListener('change', function(){inputAddr = addr.value});
            phone.addEventListener('change', function(){inputPhone = phone.value});

            var cartRef = firebase.database().ref('shoppingCart/'+user.uid);
            var order;
            var total = 0;
            cartRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    console.log(childSnapshot.key);
                    if(childData.name=='item1') amount1 = childData.amount;
                    else if(childData.name=='item2') amount2 = childData.amount;
                    else if(childData.name=='item3') amount3 = childData.amount;
                    //if(childData.name==undefined) total_post[total_post.length-1] = childData;
                    total += eval(childData.amount+'*'+childData.price);
                });

            })
            .catch(e => console.log(e.message));

            send_btn.addEventListener('click', function(){
                var d = new Date;
                var n = d.toLocaleString();
                console.log(n);
                if(inputName!=''&&inputEmail!=''&&inputAddr!=''&&inputPhone!=''){
                    order = {item1: amount1, item2: amount2, item3: amount3, sum: total, name: inputName, email: inputEmail, address: inputAddr, phonenumber: inputPhone, timestamp: n};
                    firebase.database().ref('order/'+ID).push(order);
                    cartRef.remove();
                    alert('checkout success');
                    window.location = 'profile.html';
                }
                else alert("please complete the information form");
            });

        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            window.location = 'index.html';
        }

    });

}

window.onload = function () {
    init();
}