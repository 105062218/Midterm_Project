function init() {
    var ID;
    var u = firebase.auth().currentUser;
    var menu = document.getElementById('dynamic-menu');
    var update_btn = document.getElementById('update_btn');

    var inputName = document.getElementById('Name');
    var inputOccup = document.getElementById('Occup');
    var inputAddr = document.getElementById('Addr');
    var inputPhone = document.getElementById('Phone');

    var newName = inputName.value;
    var newOccup = inputOccup.value;
    var newAddr = inputAddr.value;
    var newPhone = inputPhone.value;

    var user_name;
    var user_occup;
    var user_addr;
    var user_phone;

    var order_list = document.getElementById('order_list');
    var order = document.getElementById('order');

    

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            ID = user.uid;
            u = user;
            nameRef = firebase.database().ref('UserName/'+ID);
            /*nameRef.once("value").then(function(snapshot){
                user_name = snapshot.val().name;
                
                inputName.value = user_name;
                inputName.addEventListener('change', function(){newName = inputName.value;});
                document.getElementById('dispName').innerHTML = user_name;
                update_btn.addEventListener('click', function(){
                    firebase.database().ref('UserName/'+ID).set({name: newName}).catch(e=>alert(e));
                    document.getElementById('dispName').innerHTML = newName;
                    user.updateProfile({
                        displayName: newName}).then(function(){alert('update success');}).catch(e=>{alert(e);});
                    
                });*/
                console.log(firebase.database().ref('UserName/'+ID+'/name'));
                    nameRef.on("value", function(snapshot){

                        try {
                            user_name = snapshot.val().name;
                            user_occup = snapshot.val().occupation;
                            user_addr = snapshot.val().address;
                            user_phone = snapshot.val().phone;

                        } catch (error) {
                            user_name = '';
                            user_occup = '';
                            user_addr = '';
                            user_phone = '';
                        }

                        inputName.value = user_name;
                        if(user_name==undefined) inputAddr.value = '';

                        inputAddr.value = user_addr;
                        if(user_addr==undefined) inputAddr.value = '';

                        inputOccup.value = user_occup;
                        if(user_occup==undefined) inputOccup.value = '';

                        inputPhone.value = user_phone;
                        if(user_phone==undefined) inputPhone.value = '';

                        inputName.addEventListener('change', function(){newName = inputName.value;});
                        inputOccup.addEventListener('change', function(){newOccup = inputOccup.value;});
                        inputAddr.addEventListener('change', function(){newAddr = inputAddr.value;});
                        inputPhone.addEventListener('change', function(){newPhone = inputPhone.value;});

                        document.getElementById('dispName').innerHTML = user_name;
                        document.getElementById('dispEmail').innerHTML = user.email;
                        document.getElementById('dispOccup').innerHTML = user_occup;
                        if(user_occup==undefined) document.getElementById('dispOccup').innerHTML = '';

                        update_btn.addEventListener('click', function(){
                            if(newName!=''){
                                firebase.database().ref('UserName/'+ID).update({name: newName}).catch(e=>alert(e));
                                inputName.value = newName;
                                document.getElementById('dispName').innerHTML = newName;
                            }
                            if(newOccup!=''){
                                firebase.database().ref('UserName/'+ID).update({occupation: newOccup}).catch(e=>alert(e));
                                inputOccup.value = newOccup;
                                document.getElementById('dispOccup').innerHTML = newOccup;
                            }
                            if(newAddr!=''){
                                firebase.database().ref('UserName/'+ID).update({address: newAddr}).catch(e=>alert(e));
                                inputAddr.value = newAddr;
                            }
                            if(newPhone!=''){
                                firebase.database().ref('UserName/'+ID).update({phone: newPhone}).catch(e=>alert(e));
                                inputPhone.value = newPhone;
                            } 
                            user.updateProfile({
                                displayName: newName, }).catch(e=>{alert(e);});
                            //alert('update success');
                            
                            
                        });
                    });
                    /*inputEmail.value = user.email;
                    inputEmail.addEventListener('change', function(){newEmail = inputEmail.value;});
                    document.getElementById('dispEmail').innerHTML = user.email;
                    update_btn.addEventListener('click', function(){
                        user.updateEmail(newEmail).catch(e=>{alert(e);});
                        document.getElementById('dispEmail').innerHTML = user.email;
                    });*/

            //})
            //console.log(nameRef.name);
            menu.innerHTML = "<span class='dropdown-item'  id='profilePage'>" + user.email+"</span><span class='dropdown-item' id='shopcart'>Shopping Cart</span>" + "<span class='dropdown-item' id='logout-btn'>Logout</span>";
            var logout_button = document.getElementById('logout-btn');
            logout_button.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Sign Out!')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
            document.getElementById('shopcart').addEventListener('click', function(){window.location = 'cart.html'});
            document.getElementById('profilePage').addEventListener('click', function(){window.location = 'profile.html'});
            
            var orderRef = firebase.database().ref('order/'+user.uid);
            var all_order = "";
            var order_content = [];
            orderRef.once('value')
            .then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    console.log(childSnapshot.key);
                    var key_html = "data-key='"+childSnapshot.key+"'";
                    var sum_html = "data-sum='"+childData.sum+"'";
                    var name_html = "data-name='"+childData.name+"'";
                    var email_html = "data-email='"+childData.email+"'";
                    var addr_html = "data-addr='"+childData.address+"'";
                    var phone_html = "data-phone='"+childData.phonenumber+"'";
                    var time_html = "data-time='"+childData.timestamp+"'";
                    var items_html = "data-items='";
                    if(childData.item1!=0) items_html+="item1 x "+childData.item1+', ';
                    if(childData.item2!=0) items_html+="item2 x "+childData.item2+', ';
                    if(childData.item3!=0) items_html+="item3 x "+childData.item3;
                    items_html+="'";
                    all_order += "<a class='list-group-item' data-toggle='modal' data-target='#myModal' "+key_html+" "+items_html+" "+sum_html+" "+name_html+" "+email_html+" "+addr_html+" "+phone_html+" "+time_html+">"+childSnapshot.key+"</a>"
                    //order_content[order_content.length] = childSnapshot.key;
                });
                
                order_list.innerHTML = all_order;
                if(order_list.innerHTML=='') order_list.innerHTML = "<p class='list-group-item'>You don't have any order here.</p>"
            })
            .catch(e => console.log(e.message));

            $('#myModal').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var k = button.data('key') // Extract info from data-* attributes
                var i = "<li class='list-group-item'><strong>items: </strong>"+button.data('items')+"</li>";
                var s = "<li class='list-group-item'><strong>total: </strong>$"+button.data('sum')+"</li>";
                var n = "<li class='list-group-item'><strong>purchaser name: </strong>"+button.data('name')+"</li>";
                var e = "<li class='list-group-item'><strong>purchaser email: </strong>"+button.data('email')+"</li>";
                var a = "<li class='list-group-item'><strong>delivery address: </strong>"+button.data('addr')+"</li>";
                var p = "<li class='list-group-item'><strong>contact number: </strong>"+button.data('phone')+"</li>";
                var t = button.data('time');
                // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
                // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
                var modal = $(this)
                var listItems = i+s+n+e+a+p;
                
                modal.find('.list-group').html(listItems);
                modal.find('#time').text(t);
                modal.find('.modal-title').html("<strong>Order No.  </strong>"+k);
                //modal.find('#order_sum').text('total: '+s);
                //modal.find('.modal-body input').val(recipient)
                $('#cancel_btn').click(function(){
                    firebase.database().ref('order/'+user.uid).child(button.data('key')).remove();
                    window.location.reload();
                });
            });

            
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            window.location = 'index.html';
        }

    });

    function showOrder(key){
        
    }

    /*$('#myModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('key') // Extract info from data-* attributes
        var s = button.data('sum');
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-body').text(recipient+'    '+s)
        //modal.find('.modal-body input').val(recipient)
    });*/


}

window.onload = function () {
    init();
}