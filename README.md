# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Shopping Website
* Key functions (add/delete)
    1. view product page
    2. add item to cart
    3. checkout
    4. check orders
* Other functions (add/delete)
    1. view user profile
    2. edit user information
    3. cancel orders

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|Y|

## Website Detail Description
### URL: https://105062218.gitlab.io/Midterm_Project
### Firebase host: https://store-b6d01.firebaseapp.com/
### Homepage
<img src='public/img/img1.png' width="700px" height="400px"></img>
* Click 'Account' to login
* If a user logged in, the dropdown menu will be like this:
<br>
<img src='public/img/img7.png' width="400px" height="200px"></img>

* Click user email to go to the profile page
* Click "Shopping Cart" to view the cart
* Click "Logout"to logout
* Click the item cards to view item details and purchase

### Signin Page
<img src='public/img/img8.png' width="280px" height="200px"></img>
* Sign in with email and password or Google account
* Click "New Account" to sign up

### Item Page
<img src='public/img/img2.png' width="700px" height="400px"></img>
* Input number to change the amount
* Click "Add to cart" to add item to the cart
* The "Add to cart" button will be disabled if the user isn't logged in

### User Profile
<img src='public/img/img3.png' width="700px" height="400px"></img>
* Fill in the form and click "Confirm Change" to update the information
* Click the order No. to check details
<img src='public/img/img6.png' width="700px" height="400px"></img>
* Click "Cancel Order" to cancel the order

### Shopping Cart
<img src='public/img/img4.png' width="700px" height="400px"></img>
* Click "Checkout" to checkout
* Click the item to go to item pages and edit the purchase

### Checkout Page
<img src='public/img/img5.png' width="700px" height="400px"></img>
* edit purchaser and delivery information
* Click "Submit" to send the order to server
* Click "Back to cart" to go back to cart

## Security Report (Optional)
